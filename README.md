## Useful links
- Conduktor
  - [Learn Apache Kafka](https://www.conduktor.io/apache-kafka-for-beginners/)
  - [Starting Kafka](https://www.conduktor.io/kafka/starting-kafka/)
  - [Conduktor Alternatives](https://upstash.com/)